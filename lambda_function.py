#Stolen from AWS at https://github.com/awsdocs/aws-doc-sdk-examples/blob/main/python/example_code/lambda/lambda_handler_rest.py

import json
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event, context):

    logger.info("Request: %s", event)
    response_code = 200

    query_string = event.get('queryStringParameters')
    name = 'D. E. Fault'
    name = query_string.get('name', name)
    greeting = f"Góðan daginn, {name}. Bless bless!"

    response = {
        'statusCode': response_code,
        'body': json.dumps({'message': greeting})
    }

    logger.info("Response: %s", response)
    return response