terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/${GL_PROJECT_ID}/terraform/state/furl-demo" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/${GL_PROJECT_ID}/terraform/state/furl-demo/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${GL_PROJECT_ID}/terraform/state/furl-demo/lock" \
    -backend-config="username=${GL_USER}" \
    -backend-config="password=${GL_API_FURL}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"