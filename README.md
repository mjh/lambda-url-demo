# Lambda Url Demo

## What's going on here?

This is a little test of how the new [Lambda Function URLs](https://aws.amazon.com/blogs/aws/announcing-aws-lambda-function-urls-built-in-https-endpoints-for-single-function-microservices/) work with AWS. 

## Do it

Gitpod is the easiest way to do this demo

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/mjh/lambda-url-demo)

However, this assumes you either:

Have [Gitpod Variables](https://gitpod.io/variables) set for `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` or that you are willing to run `EXPORT AWS_ACCESS_KEY_ID=YourKeys` and `EXPORT AWS_SECRET_ACCESS_KEY=YourSecretKey`

Otherwise, in addition to this demo, you need access to an AWS account and Terraform installed - I would suggest using [tfenv](https://github.com/tfutils/tfenv)

### State File
This assumes you are using an HTTP backend, such as [Gitlab Terraform Backend](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html) but you can edit the `backend.tf` to your heart's desire. 
If you're doing this _only_ locally, you can just remove `backend.tf`, which is good for local testing.

## How to use?

Run your `terraform init` and your `terraform plan` and your `terraform apply`

Then you can run `terraform output url` and append `?name=something`.
This will do a GET and display a simple hello and goodbye with your name.

### Gitlab Specific

If you are using the Gitlab backend, you can use the `gitlab-init.sh` replacing `GL_USER` with your username, `GL_API_FURL` with an [Access Token with API](https://gitlab.com/-/profile/personal_access_tokens) and `GL_PROJECT_ID` with your Gitlab Project ID.

## TODO

- Bind another domain to the URL
- Create a more complex example (or examples?)