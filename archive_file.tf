data "archive_file" "python_lambda_package" {
  type        = "zip"
  source_file = "lambda_function.py"
  output_path = "lambda_furl_demo.zip"
}
